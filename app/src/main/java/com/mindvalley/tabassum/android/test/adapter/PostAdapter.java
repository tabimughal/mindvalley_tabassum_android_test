package com.mindvalley.tabassum.android.test.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindvalley.tabassum.android.test.R;
import com.mindvalley.tabassum.android.test.util.Alert;
import com.mindvalley.tabassum.android.test.util.ImageLoader;
import com.mindvalley.tabassum.android.test.model.Post;
import com.mindvalley.tabassum.android.test.post.PostDetailActivity;
import com.mindvalley.tabassum.android.test.userprofile.UserProfileActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.mindvalley.tabassum.android.test.util.Constants.KEY_POST;
import static com.mindvalley.tabassum.android.test.util.Constants.KEY_URL;


public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder>{

    private Context context;
    private List<Post> posts;


    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.imgMain)
        ImageView ivMain;
        @BindView(R.id.imgProfile)
        CircleImageView imgProfile;
        @BindView(R.id.tvLikes)
        TextView tvLikes;
        @BindView(R.id.layoutMain)
        LinearLayout layoutMain;
        @BindView(R.id.linearUserProfile)
        LinearLayout linearUserProfile;
        @BindView(R.id.fabDownload)
        FloatingActionButton fabDownload;
        @BindView(R.id.layoutLoader)
        LinearLayout layoutLoader;
        @BindView(R.id.imgCacel)
        ImageView imgCacel;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public PostAdapter(Context context, List<Post> posts) {
        this.context = context;
        this.posts = posts;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_post, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Post post = posts.get(position);
        holder.tvTitle.setText(post.getUser().getName());
        holder.tvTitle.setTextColor(Color.BLACK);
        holder.layoutLoader.setVisibility(View.VISIBLE);
        ImageLoader.loadImage(context, holder.imgProfile, post.getUser().getProfileImages().getSmall());
        ImageLoader.loadImageWithLoader(context, holder.ivMain, post.getUrls().getRegular(),holder.layoutLoader);
        holder.tvLikes.setText(String.valueOf(post.getLikes()));

        if (post.isLikeByUser()) {
            holder.tvLikes.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_liked), null, null, null);
        } else {
            holder.tvLikes.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_like), null, null, null);
        }
        holder.tvLikes.setTextColor(Color.BLACK);
        holder.fabDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Alert.showDownloadAlert(context);
            }
        });

        holder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchPostDetailActivit(post);
            }
        });

        holder.linearUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchPostDetailActivity(post.getUser().getLinks().getHtml());
            }
        });

    }

     private void switchPostDetailActivit(Post post){
        Intent intent = new Intent(context, PostDetailActivity.class);
        intent.putExtra(KEY_POST, post);
        context.startActivity(intent);
    }

    private void switchPostDetailActivity(String url){
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(KEY_URL, url);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

}
