package com.mindvalley.tabassum.android.test.post;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mindvalley.tabassum.android.test.R;
import com.mindvalley.tabassum.android.test.network.NetworkRequest;
import com.mindvalley.tabassum.android.test.util.ConnectionDetector;
import com.mindvalley.tabassum.android.test.adapter.PostAdapter;
import com.mindvalley.tabassum.android.test.model.Post;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private Context context;
    @BindView(R.id.rvPosts)
    RecyclerView rvPosts;
    @BindView(R.id.srPosts)
    SwipeRefreshLayout srPosts;
    @BindView(R.id.tvNoInternetConnection)
    TextView tvNoInternetConnection;

    List<Post> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        ButterKnife.bind(this);

        tvNoInternetConnection.setVisibility(ConnectionDetector.isInternetAvailable(context) ? View.GONE : View.VISIBLE);

        getPosts();
        papulatePosts(posts);

        srPosts.setOnRefreshListener(this);
    }

    private void getPosts(){
        posts = new NetworkRequest().getPostsFromNetwork(context);
    }

    private void papulatePosts(List<Post> posts) {

        srPosts.setRefreshing(false);
        if (ConnectionDetector.isInternetAvailable(context) && posts == null) {
            tvNoInternetConnection.setVisibility(View.VISIBLE);
            rvPosts.setVisibility(View.GONE);
        } else {
            if (posts != null) {
                rvPosts.setAdapter(new PostAdapter(context, posts));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(context, 1);
                rvPosts.setLayoutManager(mLayoutManager);
                rvPosts.setItemAnimator(new DefaultItemAnimator());
            } else {
                tvNoInternetConnection.setVisibility(View.VISIBLE);
                rvPosts.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onRefresh() {
        getPosts();
        papulatePosts(posts);
    }
}
