package com.mindvalley.tabassum.android.test.network;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.koushikdutta.ion.Ion;
import com.mindvalley.tabassum.android.test.model.Post;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static com.mindvalley.tabassum.android.test.util.Constants.BASE_URL;

public class NetworkRequest {

    public   List<Post> getPostsFromNetwork(Context context){
        Future<List<Post>> posts = Ion.with(context)
                .load(BASE_URL)
                .as(new TypeToken<List<Post>>() {});
        try {
            return  posts.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }
}
