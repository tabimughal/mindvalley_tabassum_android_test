package com.mindvalley.tabassum.android.test.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Tabassum on 10/29/2016.
 */

public class Category implements Serializable {

    private int id;
    private String title;
    @SerializedName("photo_count")
    private int photoCount;
    private String likes;
    private Links links;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }
}
