package com.mindvalley.tabassum.android.test.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Tabassum on 10/29/2016.
 */

public class User implements Serializable {

    private String id;
    private String userName;
    private String name;
    @SerializedName("profile_image")
    private ProfileImage profileImages;
    private Links links;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProfileImage getProfileImages() {
        return profileImages;
    }

    public void setProfileImages(ProfileImage profileImages) {
        this.profileImages = profileImages;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }
}
