package com.mindvalley.tabassum.android.test.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.DrawableCrossFadeFactory;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.mindvalley.tabassum.android.test.R;


public class ImageLoader {

    public static void loadImageWithLoader(final Context context, final ImageView imageView, final String url, final LinearLayout layoutLoader) {

        imageView.setTag(imageView.getId(), url);

        Glide.with(context)
                .load(url)
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder)
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(final Exception e, final String model, final Target<Bitmap> target, final boolean isFirstResource) {
                        layoutLoader.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Bitmap resource, final String model, final Target<Bitmap> target, final boolean isFromMemoryCache, final boolean isFirstResource) {
                        final ImageViewTarget imageViewTarget = (ImageViewTarget) target;
                        layoutLoader.setVisibility(View.GONE);
                        return new DrawableCrossFadeFactory<>()
                                .build(isFromMemoryCache, isFirstResource)
                                .animate(
                                        new BitmapDrawable(
                                                imageViewTarget.getView().getResources(),
                                                resource
                                        ),
                                        imageViewTarget
                                );
                    }
                })
                .into(imageView);
    }

    public static void loadImage(final Context context, final ImageView imageView, final String url) {

        imageView.setTag(imageView.getId(), url);

        Glide.with(context)
                .load(url)
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder)
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(final Exception e, final String model, final Target<Bitmap> target, final boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Bitmap resource, final String model, final Target<Bitmap> target, final boolean isFromMemoryCache, final boolean isFirstResource) {
                        final ImageViewTarget imageViewTarget = (ImageViewTarget) target;
                        return new DrawableCrossFadeFactory<>()
                                .build(isFromMemoryCache, isFirstResource)
                                .animate(
                                        new BitmapDrawable(
                                                imageViewTarget.getView().getResources(),
                                                resource
                                        ),
                                        imageViewTarget
                                );

                    }
                })
                .into(imageView);
    }

    public static void loadLargeImage(final Context context, final ImageView imageView, final String url) {

        imageView.setTag(imageView.getId(), url);

        Glide.with(context)
                .load(url)
                .asBitmap()
                .placeholder(R.drawable.ic_placeholder)
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(final Exception e, final String model, final Target<Bitmap> target, final boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Bitmap resource, final String model, final Target<Bitmap> target, final boolean isFromMemoryCache, final boolean isFirstResource) {
                        final ImageViewTarget imageViewTarget = (ImageViewTarget) target;
                        return new DrawableCrossFadeFactory<>()
                                .build(isFromMemoryCache, isFirstResource)
                                .animate(
                                        new BitmapDrawable(
                                                imageViewTarget.getView().getResources(),
                                                resource
                                        ),
                                        imageViewTarget
                                );
                    }
                })
                .into(imageView);
    }

}
