package com.mindvalley.tabassum.android.test.model;

import java.io.Serializable;

/**
 * Created by Tabassum on 10/29/2016.
 */

public class Links implements Serializable {
    private String self;
    private String html;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    private String photos;
    private String likes;
}
