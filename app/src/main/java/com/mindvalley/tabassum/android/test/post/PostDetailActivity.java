package com.mindvalley.tabassum.android.test.post;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mindvalley.tabassum.android.test.R;
import com.mindvalley.tabassum.android.test.util.Alert;
import com.mindvalley.tabassum.android.test.util.ImageLoader;
import com.mindvalley.tabassum.android.test.model.Post;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase;

import static com.mindvalley.tabassum.android.test.util.Constants.KEY_POST;


public class PostDetailActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.imgMain)
    ImageViewTouch imgMain;
    @BindView(R.id.fabDownload)
    FloatingActionButton fabDownload;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);
        ButterKnife.bind(this);
        Post post = (Post) getIntent().getExtras().getSerializable(KEY_POST);
        ImageLoader.loadLargeImage(PostDetailActivity.this, imgMain, post.getUrls().getRegular());
        imgMain.setDisplayType(ImageViewTouchBase.DisplayType.FIT_IF_BIGGER);
        fabDownload.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabDownload:
                Alert.showDownloadAlert(PostDetailActivity.this);
                break;
        }
    }
}
